﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kneat_SW_test_Hannu.Models
{
    class Starship
    {
        public String Name { get; set; }
        public String Model { get; set; }
        public String Manufacturer { get; set; }
        public String Cost_In_Credits { get; set; }
        public String Length { get; set; }
        public String Max_Atmosphering_Speed { get; set; }
        public String Crew { get; set; }
        public String Passengers { get; set; }
        public String Cargo_Capacity { get; set; }
        public String Consumables { get; set; }
        public String Hyperdrive_Rating { get; set; }
        public String MGLT { get; set; }
        public String Starship_Class { get; set; }
        public String[] Pilots { get; set; }
        public String[] films { get; set; }
        public String Created { get; set; }
        public String Edited { get; set; }
        public String Url { get; set; }

    }
    class Starships
    {
        public int count { get; set; }
        public String Next { get; set; }
        public String Previous { get; set; }
        public Starship[] Results { get; set; }
    }
}
