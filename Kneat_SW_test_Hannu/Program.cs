﻿using Kneat_SW_test_Hannu.Models;
using System;
using System.Linq;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Kneat_SW_test_Hannu
{
    class Program
    {
        const int cnHours = 1;
        const int cnHoursPerDay = 24 * cnHours;
        const int cnHoursPerWeek = 7 * cnHoursPerDay;
        const int cnHoursPerMonth = 30 * cnHoursPerDay;
        const int cnHoursPerYear = 12 * cnHoursPerMonth;

        private static long _distance = 0;

        static void Main(string[] args)
        {
            if (args.Count() < 1)
            {
                Console.WriteLine("Usage: Kneat_SW_test_Hannu.exe [distance]");
            }
            else
            {
                try
                {
                    _distance = (long)Math.Round(double.Parse(args[0]), 0);
                    if (_distance > 0)
                    {
                        ListStarShips();
                    }

                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                    Console.WriteLine("Usage: Kneat_SW_test_Hannu.exe [distance]");

                    return;
                }
                finally
                {
                    Console.WriteLine("Done!");

                }
            }

        }

        private static string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        private static int CalcStops(string sMGLT, string sConsumables)
        {
            int iStops = -1;

            long lMglt = -1;
            long lFactor = -1;
            long lHours = -1;

            if (!Int64.TryParse(sMGLT, out lMglt)) {
                return -1;
            }

            string[] sFactors = sConsumables.Split(' ');
            if (!Int64.TryParse(sFactors[0], out lFactor))
            {
                return -1;
            }

            if (sFactors.Length > 1)
            {
                switch (sFactors[1])
                {
                    case "hour":
                    case "hours":
                        lHours = cnHours;
                        break;

                    case "day":
                    case "days":
                        lHours = cnHoursPerDay;
                        break;

                    case "week":
                    case "weeks":
                        lHours = cnHoursPerWeek;
                        break;

                    case "month":
                    case "months":
                        lHours = cnHoursPerMonth;
                        break;

                    case "year":
                    case "years":
                        lHours = cnHoursPerYear;
                        break;

                }
            }

            if (lHours > 0)
            {
                iStops = (int)(_distance / (lMglt * lFactor * lHours));
            }

            return iStops;
        }

        private static void ListStarShips()
        {
            string sUrl = "https://swapi.co/api/starships/";
            string sRequest = "";

            do
            {
                sRequest = Get(sUrl);
                Starships starships = JsonConvert.DeserializeObject<Starships>(sRequest);

                sUrl = starships.Next;

                foreach (Starship starship in starships.Results)
                {
                    int iStops = CalcStops(starship.MGLT, starship.Consumables);

                    if (iStops < 0)
                    {
                        Console.WriteLine("Starship " + starship.Name + " can't calculate stops required");
                    }
                    else
                    {
                        Console.WriteLine("Starship " + starship.Name + " stops required: " + iStops);
                    }
                }
            }
            while (sUrl != null);

        }
    }
}
