# Kneat_SW_test_Hannu 
# Hannu Lius / test for Kneat Software, Limerick
# 20190802

This code challenge will be using an API available here: https://swapi.co/

It lists all Star War star ships, to cover a given distance, how many stops for resupply are required.

The application takes as input a distance in mega lights (MGLT).

The output is a collection of all the star ships and the total amount of stops required to make the distance between the planets.

## Notes
If there is not enough data then following message is listed: "cant calculate stops required"
Consumables is used as integer, because value can be more accurate in smaller time unit
If consumables is less than one hour it is better to stay at home
I would not use this application for navigation because "there is never a service station when you need one!"

## Wookiee
Wookiee version is not implemented because data is not valid JSON.

instead of NULL value there is value whhuanan
given URLs does not link anywhere

acaoaoakc://cohraakah.oaoo/raakah/caorarccacahakc/?wwoorcscraao=ohooooorahwowo&akrarrwo=2

## Use cases:
** 1 ** No distance parameter
```
Kneat_SW_test_Hannu.exe
=> shows instructions
Usage: Kneat_SW_test_Hannu.exe [distance]
```

** 2 ** Wrong distance parameter
```
Kneat_SW_test_Hannu.exe ziljon
=> error message
Input string was not in a correct format.
Usage: Kneat_SW_test_Hannu.exe [distance]
```

** 3 ** Negative distance parameter
```
Kneat_SW_test_Hannu.exe
=> shows instructions
Usage: Kneat_SW_test_Hannu.exe [distance]
```

** 4 ** valid command
```
Kneat_SW_test_Hannu.exe 1000000
=>
list contains
Starship 'Y-wing', stops required: 74
Starship 'Millennium Falcon', stops required: 9
Starship 'Rebel transport', stops required: 11
```

** 5 ** valid command
```
Kneat_SW_test_Hannu.exe 1E6
=>
list contains

Starship 'Y-wing', stops required: 74
Starship 'Millennium Falcon', stops required: 9
Starship 'Rebel transport', stops required: 11
```

** 6 ** valid command
```
Kneat_SW_test_Hannu.exe 1E7
=>
even Executor has to stop

Starship 'Executor', stops required: 4
```